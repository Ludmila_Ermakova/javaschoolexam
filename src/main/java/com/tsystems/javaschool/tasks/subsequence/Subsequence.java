package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public static boolean find(List x, List y) {
        // TODO: Implement the logic here
        if(x==null||y==null)
        {
            throw new IllegalArgumentException();
        }
        if(y.containsAll(x)) {
            int n=0;
            for (int i=0; i<x.size();i++) {
                    if (y.contains(x.get(i))) {
                        if(n<=y.indexOf(x.get(i))) {
                            n=y.indexOf(x.get(i));
                        }
                        else {
                            y.remove(x.get(i));
                            i--;
                            n--;
                        }
                    }
                    else {
                        return false;
                    }
            }
        }
        else {
            return false;
        }

        return true;
    }
}
