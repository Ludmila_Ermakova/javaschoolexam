package com.tsystems.javaschool.tasks.calculator;

import java.util.*;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculator {

	/**
	 * Evaluate statement represented as string.
	 *
	 * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
	 *                  parentheses, operations signs '+', '-', '*', '/'<br>
	 *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
	 * @return string value containing result of evaluation or null if statement is invalid
	 */
	public String evaluate(String statement) {
		// TODO: Implement the logic here
		if (!Revisor(statement)) {
			return null;
		}
		Stack<String> stackPol = PolNotation(statement);
		Stack<String> stackResult = new Stack<String>();
		stackResult.clear();
		String token = stackPol.pop();
		while (stackPol.size() >= 0) {
			if (!operations.contains(token)) {
				stackResult.push(token);
				token = stackPol.pop();
			} else {
				double sum = 0;
				double a = Double.parseDouble(stackResult.pop());
				double b = Double.parseDouble(stackResult.pop());
				switch (token) {
				case "+": sum = a + b;
					break;
				case "-": sum = b - a;
					break;
				case "*": sum = b * a;
					break;
				case "/": if (a == 0) {return null;}
					else {
						sum = b / a;
					}
					break;
				}
				stackResult.push(Double.toString(sum));
				if (!stackPol.isEmpty())
					token = stackPol.pop();
				else
					break;
			}
		}

		String LResult = stackResult.pop();
		double LLResult = new BigDecimal(LResult).setScale(4, RoundingMode.UP).doubleValue();
		NumberFormat nf = new DecimalFormat("#.####");
		String res = nf.format(LLResult);
		res = res.replace(',', '.');

		return res;
	}

	/**
	 * Convert statement to Reverse Polish Notation.
	 *
	 * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
	 *                  parentheses, operations signs '+', '-', '*', '/'<br>
	 *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
	 * @return reversed stack of strings as a result of converting.
	 */
	public Stack<String> PolNotation(String statement) {
		Stack<String> stackOperations = new Stack<String>();
		Stack<String> stackNotation = new Stack<String>();
		stackOperations.clear();
		stackNotation.clear();

		StringTokenizer strTokens = new StringTokenizer(statement, operations + ".", true);

		while (strTokens.hasMoreTokens()) {

			String token = strTokens.nextToken();
			if (isDigit(token)) {
				stackNotation.push(token);
			} else if (token.equals(".")) {
				stackNotation.push(stackNotation.pop() + token + strTokens.nextToken());
			} else if (operations.contains(token)) {
				if (!stackOperations.isEmpty() && !token.equals("(")) {
					if (token.equals(")")) {
						String s = stackOperations.pop();
						while (!s.equals("(")) {
							stackNotation.push(s);
							s = stackOperations.pop();
						}
					} else if (getPriority(token) > getPriority(stackOperations.peek())) {
						stackOperations.push(token);
					} else {
						while (!stackOperations.isEmpty() && getPriority(token) <= getPriority(stackOperations.peek())) {
							stackNotation.push(stackOperations.pop());
						}
						stackOperations.push(token);
					}

				} else {
					stackOperations.push(token);
				}
			}
		}

		while (!stackOperations.isEmpty()) {
			stackNotation.push(stackOperations.pop());
		}

		Collections.reverse(stackNotation);
		return stackNotation;
	}

	/**
	 * Calculates the priority operation.
	 *
	 * @param operation string containig ")(+-/*"
	 * @return integer number of priority
	 */
	public int getPriority(String operation) {
		switch (operation) {
		case "(":
		case ")": return 0;
		case "+":
		case "-": return 1;
		case "*":
		case "/": return 2;
		default: return 3;
		}
	}

	/**
	 * Validates the possibility of converting string into a digit.
	 *
	 * @param digit string
	 * @return <code>true</code> if possible, otherwise <code>false</code>
	 */
	public boolean isDigit(String digit) {
		try {
			Double.parseDouble(digit);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Validates the possibility of converting statement into a mathematical expression.
	 *
	 * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
	 *                  parentheses, operations signs '+', '-', '*', '/'<br>
	 *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
	 * @return <code>true</code> if possible, otherwise <code>false</code>
	 */
	public boolean Revisor(String statement) {
		if (statement == null || statement == "") return false;
		if (!"0123456789(".contains(String.valueOf(statement.charAt(0)))
		        || !"0123456789)".contains(String.valueOf(statement.charAt(statement.length() - 1)))) {
			return false;
		}

		int nPar = 0;
		for (int i = 0; i < statement.length(); i++) {
			if (statement.charAt(i) == '(') {
				nPar++;
			} else if (statement.charAt(i) == ')') {
				nPar--;
				if (nPar < 0) return false;
			}
		}
		if (nPar > 0) return false;

		for (int i = 1; i < statement.length() - 1; i++) {
			if ("0123456789+-*/().".contains(String.valueOf(statement.charAt(i)))) {
				if ("+-*/".contains(String.valueOf(statement.charAt(i)))) {
					if (!"0123456789)".contains(String.valueOf(statement.charAt(i - 1))) &&
					        !"0123456789(".contains(String.valueOf(statement.charAt(i - 1)))) {
						return false;
					}
				}
				if (statement.charAt(i) == '.' && (!"0123456789".contains(String.valueOf(statement.charAt(i - 1))) &&
				                                   !"0123456789".contains(String.valueOf(statement.charAt(i - 1))))) {
					return false;
				}

			} else {
				return false;
			}
		}
		return true;
	}

	private String operations = "+-*/()";

}
