package com.tsystems.javaschool.tasks.duplicates;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeSet;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        // TODO: Implement the logic here

        if(sourceFile==null||targetFile==null)
        {
            throw new IllegalArgumentException();
        }
        try {
            FileReader freader = new FileReader(sourceFile);
            BufferedReader br = new BufferedReader(freader);
            String strLine;
            ArrayList<String> strArr = new ArrayList<>();
            while ((strLine = br.readLine()) != null) {
                strArr.add(strLine);
            }
            TreeSet<String> tset = new TreeSet<String>(strArr);
            strLine = "";
            for (String line : tset) {
                strLine += line + "[" + Collections.frequency(strArr, line) + "]" + "\r\n";
            }

            FileWriter writer = new FileWriter(targetFile, true);
            BufferedWriter bufferWriter = new BufferedWriter(writer);
            bufferWriter.write(strLine);
            bufferWriter.close();

        } catch (IOException e) {
            System.out.println(e);
            return false;
        }
        return true;
    }


}
